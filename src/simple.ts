import * as express from "express";
import * as path from "path";

const app = express();

class Product {
  constructor(
    public id : number,
    public title : string,
    public price : number) {}
}

const products = [
  new Product(0, "First", 24.99),
  new Product(1, "Second", 44.99),
  new Product(3, "Third", 55.99),
];

function getProducts(): Product[] {
  return products;
}

function getProductById(id : number) : Product {
  return products.find(p => p.id === id);
}

app.use('/', express.static(path.join(__dirname, '..', 'client')));
app.use('/node_modules', express.static(path.join(__dirname, '..', 'node_modules')));
app.use('/app', express.static(path.join(__dirname, '..', 'client/app')));

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '..', 'client/simple.html'));
});

app.get('/products', (req, res) => {
  res.json(getProducts());
});

app.get('/products/:id', (req, res) => {
  res.json(getProductById(parseInt(req.params.id)));
});

app.get('/reviews', (req, res) => res.send('Got a request for reviews'));

const server = app.listen(8000, "localhost", () => {
  const {address, port} = server.address();
  console.log('Listening on %s %s', address, port);
});
