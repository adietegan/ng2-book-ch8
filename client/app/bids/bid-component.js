"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
require("rxjs/add/operator/map");
var bid_service_1 = require("./bid-service");
var BidComponent = (function () {
    function BidComponent(wsService) {
        var _this = this;
        this.wsService = wsService;
        this.subscribeToNotifications = false;
        this.status = "unsubscribed";
        this.wsService.createObservableSocket("ws://localhost:8085")
            .map(function (res) { return JSON.parse(res); })
            .subscribe(function (data) {
            _this.newBid = data;
            _this.newBid.bidTime = Date.parse(data.bidTime);
            console.log(_this.newBid);
        }, function (err) { return console.log(err); }, function () { return console.log('The bid stream is complete'); });
    }
    BidComponent.prototype.toggleBidSubscription = function () {
        this.subscribeToNotifications = !this.subscribeToNotifications;
        this.wsService.subscribeToBids(this.subscribeToNotifications);
        this.status = this.subscribeToNotifications ? "subscribed" : "unsubscribed";
    };
    return BidComponent;
}());
BidComponent = __decorate([
    core_1.Component({
        selector: 'subscriber',
        providers: [bid_service_1.BidService],
        template: "<h1>Product bids</h1>\n             <h3>Status: {{status}}</h3>\n\n              <button (click)=\"toggleBidSubscription()\">Toggle subscription to bid notifications</button><br>\n              Last bid: {{newBid?.amount | number:'1.1-2'}} <br>\n              Bid time: {{newBid?.bidTime | date: 'medium'}}\n  "
    })
], BidComponent);
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [platform_browser_1.BrowserModule],
        declarations: [BidComponent],
        bootstrap: [BidComponent]
    })
], AppModule);
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(AppModule);
